package com.ist.webservice.provider;


import javax.annotation.PostConstruct;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.stream.Collectors;

import io.spring.guides.gs_producing_web_service.Data;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author cigui
 * @date 2021/01/14 8:03 下午
 */
@Component
public class CountryRepository {
    private static final Map<String, Data> countries = new HashMap<>();
    private static final GregorianCalendar gcal = new GregorianCalendar();

    static {
        gcal.setTime(new Date());
    }

    @PostConstruct
    public void initData() throws DatatypeConfigurationException {
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);

        Data spain = new Data();
        spain.setName("Spain");
        spain.setCapital("Madrid");
        spain.setCurrency("EUR");
        spain.setPopulation(46704314);
        spain.setDate(xgcal);

        countries.put(spain.getName(), spain);

        Data poland = new Data();
        poland.setName("Poland");
        poland.setCapital("Warsaw");
        poland.setCurrency("PLN");
        poland.setPopulation(38186860);
        poland.setDate(xgcal);

        countries.put(poland.getName(), poland);

        Data uk = new Data();
        uk.setName("United Kingdom");
        uk.setCapital("London");
        uk.setCurrency("GBP");
        uk.setPopulation(63705000);
        uk.setDate(xgcal);

        countries.put(uk.getName(), uk);
    }

    public Data findCountry(String name) {
        Assert.notNull(name, "The country's name must not be null");
        return countries.get(name);
    }

    public Collection<Data> findCountrySince(XMLGregorianCalendar calendar) {
        return countries.values().stream()
            .filter(x -> x.getDate().toGregorianCalendar().after(calendar.toGregorianCalendar()))
            .collect(Collectors.toList());
    }

    public Collection<Data> findAll() {
        return countries.values();
    }

}