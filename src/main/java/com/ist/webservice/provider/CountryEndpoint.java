package com.ist.webservice.provider;

import io.spring.guides.gs_producing_web_service.FetchAllDataRequest;
import io.spring.guides.gs_producing_web_service.FetchAllDataResponse;
import io.spring.guides.gs_producing_web_service.FetchDataRequest;
import io.spring.guides.gs_producing_web_service.FetchDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * @author cigui
 * @date 2021/01/14 8:33 下午
 */
@Endpoint
public class CountryEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private final CountryRepository countryRepository;

    @Autowired
    public CountryEndpoint(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "fetchDataRequest")
    @ResponsePayload
    public FetchDataResponse fetchDataSince(@RequestPayload FetchDataRequest request) {
        FetchDataResponse response = new FetchDataResponse();
        response.getData().addAll(countryRepository.findCountrySince(request.getSince()));
        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "fetchAllDataRequest")
    @ResponsePayload
    public FetchAllDataResponse fetchAllData(@RequestPayload FetchAllDataRequest request) {
        FetchAllDataResponse response = new FetchAllDataResponse();
        response.getData().addAll(countryRepository.findAll());
        return response;
    }
}
